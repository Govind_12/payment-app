import http from "../http-common";

class UserDetailService {
    getAll(){
        return http.get("/user-detail");
    }
    get(id){
        return http.get("/user-detail/${id}");
    }
    save(data){
        return http.post("/user-detail", data);
    }
    update(id, data){
        return http.post("/user-detail/${id}", data);
    }
    delete(id){
        return http.get("/user-detail/${id}");
    }
    deleteAll(){
        return http.get("/user-detail");
    }
}

export default new UserDetailService();
