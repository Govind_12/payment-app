import axios from "axios";

export default axios.create({
    baseURL: "http://localhost:5000",
    headers: {
        "Content-type": "application/json",
        "X-AUTH-TOKEN": "eyJhbGciOiJIUzUxMiJ9.eyJhdWQiOiJEZW1vIG1haW4iLCJlaWQiOjEsImVtbCI6ImluZm9AdHVybmluZ2Nsb3VkLmNvbSIsIlgtVEVOQU5ULUlEIjoiZDE2YzE0OTkxOTQzM2RiY2Q0YmE1YzMyNTVhZThmZTFjOTA2YTk5Yjk2N2Y1MTkzZjE5ZWNkNGJkNGI1MTkwMSIsIkJVQ0tFVCI6InRjbG91ZC1zdXBwbHltaW50LWRldmVsb3AtY29tIiwiaXNzIjoiU3VwcGx5TWludCIsImlwYSI6IklQQSIsImV4cCI6MTU1OTIyNjQyOSwicHJuIjoic3VtZWV0YSIsImp0aSI6M30._q5klrasadrML4Wo3SdmPEdlVAluH_hZjmlHgAWgbaWFdOFHNW0vOkm1RE4SKr9JoUZSy5kvhkvff1ic88tLrg"
    }
});
