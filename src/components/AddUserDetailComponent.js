import React, {Component} from "react";
import UserDetailService from "../services/UserDetailService";

export default class AddUserDetailComponent extends Component{

    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeAmmount = this.onChangeAmmount.bind(this);
        this.saveDetail = this.saveDetail.bind(this);
        this.newUserDetail = this.newUserDetail.bind(this);

        this.state = {
            id : null,
            name : "",
            ammount : "",
            submitted : false
        };
    }
    onChangeName(e){
        this.setState({
            name: e.target.value
        });
    }
    onChangeAmmount(e){
        this.setState({
            ammount: e.target.value
        });
    }
    saveDetail(){
      var data = {
          name: this.state.name,
          ammount: this.state.ammount
      };

      UserDetailService.save(data)
       .then(response => {
           this.setState({
               id : response.data.id,
               name : response.data.name,
               ammount : response.data.ammount,
               submitted : true
           });
           console.log(response.data);
       })
       .catch(e => {
           console.log(e);
       })
    }
    newUserDetail(){
        this.setState({
            id: null,
            name: "",
            ammount: "",
            submitted: false
        })
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>You submitted successfully !</h4>
                        <button className="btn btn-success" onClick={this.newUserDetail}>
                            Add
                        </button>
                    </div>
                ) : (
                    <div>
                        <div className="form-group">
                            <label htmlFor="name">Name :</label>
                            <input type="text" className="form-control" id="name" required value={this.state.name} onChange={this.onChangeName} name ="name" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="ammount">Ammount :</label>
                            <input type="text" className="form-control" id="ammount" required value={this.state.ammount} onChange={this.onChangeAmmount} name="ammount"/>
                        </div>
                        <button onClick={this.saveDetail} className="btn btn-success">
                            Submit
                        </button>
                    </div>
                )}
            </div>
        );
    }
}