import React, {Component} from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import "./App.css";
import AddUserDetail from "./components/AddUserDetailComponent";
import EditUserDetail from "./components/EditUserDetailComponent";
import ShowUserDetail from "./components/ShowUserDetailComponent";

class App extends Component{

  render(){
    return (
         <Router>
           <div>
             <nav className="navbar navbar-expand navbar-dark bg-dark">
               <a href="/user-detail" className="navbar-brand">
                   User    
               </a>

             </nav>

             <div className="container mt-3">
               <Switch>
                 <Route exact path={["/", "/user-detail"]} component={ShowUserDetail}/>
                 <Route exact path="/add/user-detail" component={AddUserDetail}/>
                 <Route exact path="/user-detail/:id" component={EditUserDetail}/>
               </Switch>
             </div>
           </div>
         </Router> 
    );
  }

}
    // <Router>
    //   <div>
    //     <nav className="navbar navbar-expand navbar-dark bg-dark">
    //       <a href="/tutorials" className="navbar-brand">
    //         bezKoder
    //       </a>
    //       <div className="navbar-nav mr-auto">
    //         <li className="nav-item">
    //           <Link to={"/tutorials"} className="nav-link">
    //             Tutorials
    //           </Link>
    //         </li>
    //         <li className="nav-item">
    //           <Link to={"/add"} className="nav-link">
    //             Add
    //           </Link>
    //         </li>
    //       </div>
    //     </nav>


export default App;
